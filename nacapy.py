#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 20 16:20:00 2018

@author: orpc
"""

import numpy as np

class Airfoil():
    """
    Class for storing and analyzing the geometry of airfoil profiles.
    """
    def __init__(self):
        """
        Initialize instance.
        """
        self.x_upper = 0
        self.z_upper = 0
        self.x_lower = 0
        self.z_lower = 0
        self.h = 0
        
    def import_coordinates(self, x_upper, z_upper, x_lower, z_lower):
        """ Imports airfoil coordinates."""
        self.x_upper = x_upper
        self.z_upper = z_upper
        self.x_lower = x_lower
        self.z_lower = z_lower
        
        
    def x_spacing_linear(self, npt):
        """
        Generates a linear spacing of ordinates from leading to trailing edge
        for an airfoil with a chord length of one.
        """
        return np.linspace(0, 1, npt)
    
    
    def x_spacing_cosine(self, npt):
        """
        Generates a cosinal spacing of ordinates from the leading to trailing
        edge for an airfoil with a chord length of one.  Cosinal spacing has 
        reduced distance between points at the leading and trailing edges.
        """
        beta = np.linspace(0, np.pi, npt)
        return (0.5*(1 - np.cos(beta)))
    
    
    def x_spacing_half_cosine(self, npt):
        """
        Generates a half-cosinal spacing of ordinates from the leading to 
        trailing edge for an airfoil with a chord length of one.  Half-cosinal 
        spacing has reduced distance between points at the leading edge only.
        """
        beta = np.linspace(0, 0.5*np.pi, npt)
        return 1 - np.cos(beta)
        
    
    def scale(self, X, Z=None):
        """
        Scale the airfoil by X and Y in the x-direction and y-direction 
        respectively.  Default value of Y is the same as X.
        """
        if Z is None:
            Z = X
        self.x_upper = self.x_upper * X
        self.z_upper = self.z_upper * Z
        self.x_lower = self.x_lower * X
        self.z_lower = self.z_lower * Z

      
    def translate(self, vector):
        """
        Translates the airfoil coordinates by the vector provided.  Vector 
        is provided as a tuple or list of two values: (X, Y).
        """
        self.x_upper = self.x_upper + vector[0]
        self.z_upper = self.z_upper + vector[1]
        self.x_lower = self.x_lower + vector[0]
        self.z_lower = self.z_lower + vector[1]
    
    
    def mirror(self):
        """
        Mirror the airfoil about the X axis
        """
        self.z_upper = -self.z_upper
        self.z_lower = -self.z_lower
        
    
    def rotate(self, angle, centroid=[0,0]):
        """
        Rotates the airfoil about the vector [0, 1, 0] centered on the
        centroid.  Centroid, by default, is at the origin.
        """
        angle = np.radians(angle)
        self.x_upper = (np.cos(angle)*(self.x_upper-centroid[0]) 
                      - np.sin(angle)*(self.z_upper-centroid[1]))
        self.z_upper = (np.sin(angle)*(self.x_upper-centroid[0]) 
                      + np.cos(angle)*(self.z_upper-centroid[1]))
        self.x_lower = (np.cos(angle)*(self.x_lower-centroid[0]) 
                      - np.sin(angle)*(self.z_lower-centroid[1]))
        self.z_lower = (np.sin(angle)*(self.x_lower-centroid[0]) 
                      + np.cos(angle)*(self.z_lower-centroid[1]))
    
    
    def set_shell_thickness(self, h):
        """
        Sets the shell thickness of the airfoil.  If set to zero, the airfoil
        is solid.
        """
        self.h = h
    
    
    def te(self):
        """
        Returns the size of trailing edge gap.
        """
        te = np.sqrt((self.z_upper[-1] - self.z_lower[-1])**2 
                     - (self.x_upper[-1] - self.x_lower[-1])**2)
        return te
        
    
    def offset(self):
        """
        Offsets a curve defined by vectors x and y by the distance h.
        """
        def ccw(x1, z1, x2, z2, x3, z3):
            """
            Test whether the turn formed by three points (x1, z1), (x2, z2), 
            (x3, z3) is counter-clockwise.
            """
            return (x2 - x1)*(z3 - z1) > (z2 - z1)*(x3 - x1)
        
        
        def intersects(x1, z1, x2, z2):
            """ Returns True if two line segments intersect."""
            return ((ccw(x1[0], z1[0], x1[1], z1[1], x2[0], z2[0]) != 
                     ccw(x1[0], z1[0], x1[1], z1[1], x2[1], z2[1])) and 
                    (ccw(x2[0], z2[0], x2[1], z2[1], x1[0], z1[0]) != 
                     ccw(x2[0], z2[0], x2[1], z2[1], x1[1], z1[1])))
        
        
        def offset_curve(x, z, h):
            """
            Offset a curve.
            """           
            # find a vector perpendicular to each segment
            vec_perp_x = -(z[1:] - z[:-1])
            vec_perp_z =  (x[1:] - x[:-1])
            norm = np.sqrt(vec_perp_x**2 + vec_perp_z**2)
            vec_perp_x = vec_perp_x / norm * -h
            vec_perp_z = vec_perp_z / norm * -h
            # find the avgerage vector at each node
            curve_length = len(x)
            vec_x = np.zeros(curve_length)
            vec_x[:-1] += vec_perp_x
            vec_x[1:] += vec_perp_x
            vec_x[1:-1] = vec_x[1:-1] * 0.5
            vec_z = np.zeros(curve_length)
            vec_z[:-1] += vec_perp_z
            vec_z[1:] += vec_perp_z
            vec_z[1:-1] = vec_z[1:-1] * 0.5
            # create the new vectors
            x_new = x + vec_x
            z_new = z + vec_z
        
            return [x_new, z_new]

        
        def fix_intersections(x1, z1, x2, z2):
            """
            Find intersections and trim two curves.
            """
            i_intersects = []
            j_intersects = []
            for i in range(len(x1) - 1):
                seg_x1 = x1[i:i+2]
                seg_z1 = z1[i:i+2]
                for j in range(len(x2) - 1):
                    seg_x2 = x2[j:j+2]
                    seg_z2 = z2[j:j+2]
                    #  check for an intersection
                    if intersects(seg_x1, seg_z1, seg_x2, seg_z2):
                        i_intersects.append(i)
                        j_intersects.append(j)
            # trim the vectors
            x1 = x1[i_intersects[0]:i_intersects[1]+2]
            z1 = z1[i_intersects[0]:i_intersects[1]+2]
            x2 = x2[j_intersects[0]:j_intersects[1]+2]
            z2 = z2[j_intersects[0]:j_intersects[1]+2]
            
            # find the first intersections
            seg_x1 = x1[0:2]
            seg_z1 = z1[0:2]
            seg_x2 = x2[0:2]
            seg_z2 = z2[0:2]
            m1 = (seg_z1[1] - seg_z1[0]) / (seg_x1[1] - seg_x1[0])
            m2 = (seg_z2[1] - seg_z2[0]) / (seg_x2[1] - seg_x2[0])
            b1 = seg_z1[0] - m1 * seg_x1[0]
            b2 = seg_z2[0] - m2 * seg_x2[0]
            x_intersection0 = (b2 - b1) / (m1 - m2)
            z_intersection0 = m1 * x_intersection0 + b1
            x1[0] = x_intersection0
            x2[0] = x_intersection0
            z1[0] = z_intersection0
            z2[0] = z_intersection0
            
            # find the first intersections
            seg_x1 = x1[-2:]
            seg_z1 = z1[-2:]
            seg_x2 = x2[-2:]
            seg_z2 = z2[-2:]
            m1 = (seg_z1[1] - seg_z1[0]) / (seg_x1[1] - seg_x1[0])
            m2 = (seg_z2[1] - seg_z2[0]) / (seg_x2[1] - seg_x2[0])
            b1 = seg_z1[0] - m1 * seg_x1[0]
            b2 = seg_z2[0] - m2 * seg_x2[0]
            x_intersection1 = (b2 - b1) / (m1 - m2)
            z_intersection1 = m1 * x_intersection1 + b1
            x1[-1] = x_intersection1
            x2[-1] = x_intersection1
            z1[-1] = z_intersection1
            z2[-1] = z_intersection1
            
            return [x1, z1, x2, z2]

        
        # join the two curves
        xi_upper, zi_upper = offset_curve(self.x_upper, self.z_upper, self.h)
        xi_lower, zi_lower = offset_curve(self.x_lower, self.z_lower, -self.h)
        xi_upper, zi_upper, xi_lower, zi_lower = fix_intersections(xi_upper, zi_upper, xi_lower, zi_lower)
        
        return [ xi_upper, zi_upper, xi_lower, zi_lower ]
    
    
    def geom_prop(self, x1, z1, x2, z2):
        """
        Returns the area and centroid of polygon described two curves.
        """
        x = np.hstack([x2[::-1], x1])
        z = np.hstack([z2[::-1], z1])
        A = 0.5 * np.sum(x * np.roll(z,1) - z*np.roll(x,1))
        Cx = (1/(6*A)) * np.sum((x+np.roll(x,1))*(x*np.roll(z,1)
                                                 -z*np.roll(x,1)))
        Cz = (1/(6*A)) * np.sum((z+np.roll(z,1))*(x*np.roll(z,1)
                                                 -z*np.roll(x,1)))
        
        Ixx = (1/12)*np.sum(((z-Cz)**2 + (z-Cz)*np.roll((z-Cz),1)
                   + np.roll((z-Cz),1)**2) * ((x-Cx)*np.roll((z-Cz),1)
                   - (z-Cz)*np.roll((x-Cx),1)))
        Izz = (1/12)*np.sum(((x-Cx)**2 + (x-Cx)*np.roll((x-Cx),1)
                   + np.roll((x-Cx),1)**2) * ((x-Cx)*np.roll((z-Cz),1)
                   - (z-Cz)*np.roll((x-Cx),1)))
        Ixz = (1/24)*np.sum(((x-Cx)*np.roll((z-Cz),1) + 2*(x-Cx)*(z-Cz)
                   + 2*np.roll((x-Cx),1)*np.roll((z-Cz),1)
                   + (z-Cz)*np.roll((x-Cx),1))*((x-Cx)*np.roll((z-Cz),1)
                   - (z-Cz)*np.roll((x-Cx),1)))
        return [A, Cx, Cz, Ixx, Izz, Ixz]
    
    
    def area(self):
        """
        Calculate the  cross-sectional area.
        """
        A, Cx, Cz, Ixx, Izz, Ixz = self.geom_prop(self.x_upper, self.z_upper, 
                                   self.x_lower, self.z_lower)
        if self.h > 0:
            xi_upper, zi_upper, xi_lower, zi_lower = self.offset()
            Ai,Cxi,Czi,Ixxi,Izzi,Ixzi = self.geom_prop(xi_upper, zi_upper, 
                                                       xi_lower, zi_lower)
            A -= Ai
        return A
    

    def xbar(self):
        """
        Calculate the neutral bending axis about the x-axis.
        """
        A, Cx, Cz, Ixx, Izz, Ixz = self.geom_prop(self.x_upper, self.z_upper, 
                                   self.x_lower, self.z_lower)
        if self.h > 0:
            xi_upper, zi_upper, xi_lower, zi_lower = self.offset()
            Ai,Cxi,Czi,Ixxi,Izzi,Ixzi = self.geom_prop(xi_upper, zi_upper, 
                                                       xi_lower, zi_lower)
            Cx = (Cx*A - Cxi*Ai) / (A-Ai)
        return Cx
    
    
    def zbar(self):
        """
        Calculates the geometric centroid of the airfoil.
        """
        A, Cx, Cz, Ixx, Izz, Ixz = self.geom_prop(self.x_upper, self.z_upper, 
                                   self.x_lower, self.z_lower)
        if self.h > 0:
            xi_upper, zi_upper, xi_lower, zi_lower = self.offset()
            Ai,Cxi,Czi,Ixxi,Izzi,Ixzi = self.geom_prop(xi_upper, zi_upper, 
                                                       xi_lower, zi_lower)
            Cz = (Cz*A - Czi*Ai) / (A-Ai)
        return Cz
    
    
    def Ixx(self):
        """
        Returns the 2nd Area Moment (moment of inertia) of the airfoil cross-
        section.
        """
        A, Cx, Cz, Ixx, Izz, Ixz = self.geom_prop(self.x_upper, self.z_upper, 
                                   self.x_lower, self.z_lower)
        if self.h > 0:
            xi_upper, zi_upper, xi_lower, zi_lower = self.offset()
            Ai,Cxi,Czi,Ixxi,Izzi,Ixzi = self.geom_prop(xi_upper, zi_upper, 
                                                       xi_lower, zi_lower)
            Ixx += Ixxi
        return Ixx
    

    def Izz(self):
        """
        Returns the 2nd Area Moment (moment of inertia) of the airfoil cross-
        section.
        """
        A, Cx, Cz, Ixx, Izz, Ixz = self.geom_prop(self.x_upper, self.z_upper, 
                                   self.x_lower, self.z_lower)
        if self.h > 0:
            xi_upper, zi_upper, xi_lower, zi_lower = self.offset()
            Ai,Cxi,Czi,Ixxi,Izzi,Ixzi = self.geom_prop(xi_upper, zi_upper, 
                                                       xi_lower, zi_lower)
            Izz += Izzi
        return Izz


    def Ixz(self):
        """
        Returns the 2nd Area Moment (moment of inertia) of the airfoil cross-
        section.
        """
        A, Cx, Cz, Ixx, Izz, Ixz = self.geom_prop(self.x_upper, self.z_upper, 
                                   self.x_lower, self.z_lower)
        if self.h > 0:
            xi_upper, zi_upper, xi_lower, zi_lower = self.offset()
            Ai,Cxi,Czi,Ixxi,Izzi,Ixzi = self.geom_prop(xi_upper, zi_upper, 
                                                       xi_lower, zi_lower)
            Ixz += Ixzi
        return Ixz
        
    
    def export_csv(self, foilname):
        """
        Exports the airfoil coordinates to a plain csv file.
        """
        x = np.hstack([self.x_upper[::-1], self.x_lower[1:]])
        z = np.hstack([self.z_upper[::-1], self.z_lower[1:]])
        val = np.vstack([x, z]).T
        fname = foilname + '.txt'
        np.savetxt(fname, val, delimiter=",",  fmt='%1.10f')
        
            
    def export_rhino(self, foilname):
        """
        Exports airfoil coordinates for importing into the Rhino3D CAD program.
        """
        x = np.hstack([self.x_upper[::-1], self.x_lower[1:]])
        z = np.hstack([self.z_upper[::-1], self.z_lower[1:]])
        val = np.vstack([x, z]).T

        fname = foilname + '.txt'
        header = "InterpCrv"
        footer = "Enter"

        np.savetxt(fname, val, header=header, footer=footer, delimiter=",",
                   comments="", fmt='%1.10f')
        
        
class FourDigit(Airfoil):
    """
    Class for generating airfoils from the NACA four digit series.
    """
 
    def four_digit_thickness(self, x, t, TE):
        """
        Returns the 4-digit-series thickness distribution for an airfoil with
        a chord length of one.
        """
        A0 =  0.2969
        A1 = -0.1260
        A2 = -0.3516
        A3 =  0.2843
        if self.TE is np.nan:
            A4 = -0.1015
        else:
            A4 = -0.1036
        
        return 5 * t * (A0*np.sqrt(x) + A1*x + A2*x**2 + A3*x**3 + A4*x**4)
        

    def two_digit_camberline(self, x, p, m):
        """
        Returns the two digit series camber line for an airfoil with a chord
        length of one.
        """
        y = np.zeros(len(x))
        if m > 0:
            y_front = p * ((2*x/m) - (x/m)**2 )
            y_back = p * (1 - 2*m + 2*m*x - x**2) / (1 - m)**2
            y[x < m] = y_front[x < m]
            y[x >= m] = y_back[x >= m]
    
        return y


    def gen_unit_chord_airfoil(self, spacing):
        """
        Generates a unit chord length airfoil.
        """
        p = int(self.NACA[0]) / 100
        m = int(self.NACA[1]) / 10
        t = int(self.NACA[2:]) / 100
        
        if spacing is "linear":
            x = self.x_spacing_linear(self.npt)
        elif spacing is "cosine":    
            x = self.x_spacing_cosine(self.npt)
        elif spacing is "half_cosine":
            x = self.x_spacing_half_cosine(self.npt)
                
        y_t = self.four_digit_thickness(x, t, self.TE)
        y_c = self.two_digit_camberline(x, p, m)
        
        self.x_upper = x
        self.z_upper = y_c + y_t
        self.x_lower = x
        self.z_lower = y_c - y_t


    def __init__(self, NACA, ch, TE=np.nan, npt=120, h=0, spacing="half_cosine"):
        """
        Initializes a new 4 digit airfoil.
        """
        Airfoil.__init__(self)
        self.NACA = NACA
        self.ch = ch
        self.TE = TE
        self.npt = int(npt / 2)
        self.h = h
        # run functions        
        self.gen_unit_chord_airfoil(spacing)
        self.scale(ch)
        
        
class FourDigitMod(FourDigit):
    """
    Class for generating airfoils from the NACA modified four digit series.
    """
    
    def __init__(self, NACA, ch, TE=np.nan, npt=120, h=0, spacing="half_cosine"):
        """
        Initializes a new modified 4 digit airfoil.
        """
        self.NACA = NACA[:4]
        self.NACAMOD = NACA[-2:]
#        FourDigit.__init__(self)
        
        
if __name__ == "__main__":
    
    def four_digit_thickness_mod(x, t, I, T):
        """
        Get thickness distribution for the four digit modified airfoil series
        """
        d0 = 0.002
        if T == 0.2:
            d1 = 0.2
        elif T == 0.3:
            d1 = 0.234
        elif T == 0.4:
            d1 = 0.315
        elif T == 0.5:
            d1 = 0.465
        elif T == 0.6:
            d1 = 0.700
        d2 = (0.294 - 2*(1-T)*d1) / (1-T)**2
        d3 = (-0.196 + (1-T)*d1) / (1-T)**3
        
        if I <= 8:
            a0 = 0.296904 * (I / 6)
        else:
            a0 = 10.3933
            
        rho1 = (1/5)*((1-T)**2 / (0.588 - 2*d1*(1-T)))
        a1 = (0.3/T) - (15/8)*(a0/np.sqrt(T)) - (T/(10*rho1))
        a2 = (-0.3/T**2) + (5/4)*(a0/(T**(3/2))) + (1/(5*rho1))
        a3 = (0.1/T**3) - (0.375*a0)/(T**(5/2)) - 1/(10*rho1*T)
        
        y_t = np.zeros(len(x))
        y_t_front = (5*t)*(a0*np.sqrt(x) + a1*x + a2*x**2 + a3*x**3)
        y_t_back = (5*t)*(d0 + d1*(1-x) + d2*(1-x)**2 + d3*(1-x)**3)
        y_t[x < T] = y_t_front[x < T]
        y_t[x >= T] = y_t_back[x >= T]
        
        return y_t
        
#    f = Airfoil()
#    x = f.x_spacing_half_cosine(120)
#    yt  = four_digit_thickness_mod(x, 0.2, 4, 0.4)
#    yt2 = four_digit_thickness_mod(x, 0.2, 6, 0.3)
    
    f = FourDigit("1523", 1, TE=0)
    f.export_csv("casename")
    print(f.te())
    
    x_mod = f.x_lower
    y_mod = (0.03/2)*x_mod**2
    
    import matplotlib.pyplot as plt
    
    plt.figure(dpi=100)
    plt.plot(f.x_upper, f.z_upper, ":", color="C0")
    plt.plot(f.x_lower, f.z_lower, ":", color="C1")
    plt.plot(f.x_upper, f.z_upper+y_mod, color="C0")
    plt.plot(f.x_lower, f.z_lower-y_mod, color="C1")
    plt.axis("equal")
#    

    